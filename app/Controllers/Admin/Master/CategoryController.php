<?php

namespace App\Controllers\Admin\Master;

use App\Services\CategoryService;
use App\Controllers\BaseController;

class CategoryController extends BaseController{

	private $category_service;

	public function __construct(){
		$this->category_service = new CategoryService;
	}

	public function setSessionData($message = '', $session = false, $type = 'success', $custom = false){
		$session = session();

		$session->setFlashdata('message_alert', $message);
		$session->setFlashdata('session_alert', $session);
		$session->setFlashdata('type_alert', $type);
		$session->setFlashdata('custom_alert', $custom);
	}

	public function checkLoggedUser(){
		$session = session();
		if ($session->get('login_admin_session')) {
			return true;
		}else{
			return false;
		}
	}

	public function index()
	{
		if (!$this->checkLoggedUser()) {
			return redirect()->route('admin.login');
		}
		$session = session();
		$category = $this->category_service->getAllCategory();

		return view('admin/master/category/index', [
			'session' => $session,
			'category' => $category
		]);
	}

	
}
