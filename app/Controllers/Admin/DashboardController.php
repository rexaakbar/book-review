<?php

namespace App\Controllers\Admin;

use App\Services\AuthAdminService;
use App\Controllers\BaseController;

class DashboardController extends BaseController{

	private $auth_admin_service;

	public function __construct(){
		$this->auth_admin_service = new AuthAdminService;
	}

	public function setSessionData($message = '', $session = false, $type = 'success', $custom = false){
		$session = session();

		$session->setFlashdata('message_alert', $message);
		$session->setFlashdata('session_alert', $session);
		$session->setFlashdata('type_alert', $type);
		$session->setFlashdata('custom_alert', $custom);
	}

	public function checkLoggedUser(){
		$session = session();
		if ($session->get('login_admin_session')) {
			return true;
		}else{
			return false;
		}
	}

	public function index()
	{
		if (!$this->checkLoggedUser()) {
			return redirect()->route('admin.login');
		}
		$session = session();
		return view('admin/dashboard/index', [
			'session' => $session
		]);
	}
}
