<?php

namespace App\Controllers\Admin;

use App\Services\AuthAdminService;
use App\Controllers\BaseController;

class AuthController extends BaseController{

	private $auth_admin_service;

	public function __construct(){
		$this->auth_admin_service = new AuthAdminService;
	}

	public function setSessionData($message = '', $session = false, $type = 'success', $custom = false){
		$session = session();

		$session->setFlashdata('message_alert', $message);
		$session->setFlashdata('session_alert', $session);
		$session->setFlashdata('type_alert', $type);
		$session->setFlashdata('custom_alert', $custom);
	}

	public function checkLoggedUser(){
		$session = session();
		if ($session->get('login_admin_session')) {
			return false;
		}else{
			return true;
		}
	}

	public function login()
	{
		if (!$this->checkLoggedUser()) {
			return redirect()->route('admin.dashboard');
		}
		$session = session();
		return view('admin/auth/login', [
			'session' => $session
		]);
	}

	public function login_store(){
		try {
			$res_auth = $this->auth_admin_service->login($this->request->getVar('email'), $this->request->getVar('password'));

			if ($res_auth['state']) {
				$this->setSessionData($res_auth['message'], true, 'success');
				return redirect()->route('admin.dashboard');
			}else{
				$this->setSessionData($res_auth['message'], true, 'warning');
				return redirect()->route('admin.login');
			}
		} catch (\Exception $e) {
			$this->setSessionData($e->getMessage(), true, 'warning');
			return redirect()->route('admin.login');
		}
	}

	public function logout_store(){
		try {
			$res_auth = $this->auth_admin_service->logout();

			if ($res_auth['state']) {
				$this->setSessionData($res_auth['message'], true, 'success');
				return redirect()->route('admin.dashboard');
			}else{
				$this->setSessionData($res_auth['message'], true, 'warning');
				return redirect()->back();
			}
		} catch (\Exception $e) {
	        $this->setSessionData($e->getMessage(), true, 'warning');
	        dd($e->getMessage());
			return redirect()->back();
		}
	}
}
