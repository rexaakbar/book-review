<?php

namespace App\Controllers;

use App\Services\AuthService;

class AuthController extends BaseController{

	private $auth_service;

	public function __construct(){
		$this->auth_service = new AuthService;
	}

	public function setSessionData($message = '', $session = false, $type = 'success', $custom = false){
		$session = session();

		$session->setFlashdata('message_alert', $message);
		$session->setFlashdata('session_alert', $session);
		$session->setFlashdata('type_alert', $type);
		$session->setFlashdata('custom_alert', $custom);
	}

	public function checkLoggedUser(){
		$session = session();
		if ($session->get('login_session')) {
			return false;
		}else{
			return true;
		}
	}

	public function login()
	{
		if (!$this->checkLoggedUser()) {
			return redirect()->route('home');
		}
		$session = session();
		return view('auth/login', [
			'session' => $session
		]);
	}

	public function login_store(){
		try {
			$res_auth = $this->auth_service->login($this->request->getVar('email'), $this->request->getVar('password'));

			if ($res_auth['state']) {
				$this->setSessionData($res_auth['message'], true, 'success');
				return redirect()->route('home');
			}else{
				$this->setSessionData($res_auth['message'], true, 'warning');
				return redirect()->route('login');
			}
		} catch (\Exception $e) {
			$this->setSessionData($e->getMessage(), true, 'warning');
			return redirect()->back();
		}
	}

	public function register(){
		if (!$this->checkLoggedUser()) {
			return redirect()->route('home');
		}
		$session = session();
		return view('auth/register', [
			'session' => $session,
			'validation' => null
		]);
	}

	public function register_store(){
		try {
			//include helper form
	        helper(['form']);
	        //set rules validation form
	        $rules = [
	            'username'      => ['label' => 'Username', 'rules' => 'required|min_length[3]|max_length[20]'],
	            'email'         => ['label' => 'Email', 'rules' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[users.email]'],
	            'password'      => ['label' => 'Password', 'rules' => 'required|min_length[6]|max_length[200]'],
	            'confpassword'  => ['label' => 'Confirm Password', 'rules' => 'matches[password]']
	        ];

	        if($this->validate($rules)){
	            $res_auth = $this->auth_service->register($this->request->getVar());

	            if ($res_auth['state']) {
	            	$this->setSessionData($res_auth['message'], true, 'success');
	            	$this->auth_service->login($this->request->getVar('email'), $this->request->getVar('password'));
	            	return redirect()->route('home');
	            }else{
	            	$this->setSessionData($res_auth['message'], true, 'warning');
	            	return redirect()->route('register');
	            }
	        }else{
	            $this->setSessionData('Register Failed', true, 'warning', $this->validator->getErrors());
	            return redirect()->route('register');
	        }
		} catch (\Exception $e) {
	        $this->setSessionData($e->getMessage(), true, 'warning');
	        dd($e->getMessage());
			return redirect()->back();
		}
	}

	public function logout_store(){
		try {
			$res_auth = $this->auth_service->logout();

			if ($res_auth['state']) {
				$this->setSessionData($res_auth['message'], true, 'success');
				return redirect()->route('home');
			}else{
				$this->setSessionData($res_auth['message'], true, 'warning');
				return redirect()->route('home');
			}
		} catch (\Exception $e) {
	        $this->setSessionData($e->getMessage(), true, 'warning');
	        dd($e->getMessage());
			return redirect()->back();
		}
	}
}
