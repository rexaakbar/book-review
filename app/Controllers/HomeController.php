<?php

namespace App\Controllers;

use App\Services\BookService;
use App\Services\CategoryService;
use App\Services\AuthorService;

class HomeController extends BaseController
{
	private $book_service, $category_service, $author_service;

	public function __construct(){
		$this->book_service = new BookService;
		$this->category_service = new CategoryService;
		$this->author_service = new AuthorService;
	}

	public function index()
	{
		$session = session();

		$book = $this->book_service->getAllBookDashboard();
		
		return view('home/index', [
			'session' => $session,
			'book' => $book
		]);
	}
}
