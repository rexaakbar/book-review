<?php

namespace App\Controllers;

use App\Services\BookService;
use App\Services\CategoryService;
use App\Services\ReviewService;
use App\Services\AuthService;
use App\Services\AuthorService;

class AuthorController extends BaseController
{	
	private $author_service, $book_service, $category_service, $review_service, $auth_service, $is_logged;

	public function __construct(){
		$this->author_service = new AuthorService;
		$this->book_service = new BookService;
		$this->category_service = new CategoryService;
		$this->review_service = new ReviewService;
		$this->auth_service = new AuthService;
		$this->is_logged = $this->auth_service->checkLoggedUser();
	}

	public function setSessionData($message = '', $session = false, $type = 'success', $custom = false){
		$session = session();

		$session->setFlashdata('message_alert', $message);
		$session->setFlashdata('session_alert', $session);
		$session->setFlashdata('type_alert', $type);
		$session->setFlashdata('custom_alert', $custom);
	}

	public function detail($Slug = '')
	{
		$session = session();

		$author = $this->author_service->getAuthorBySlug($Slug);
		$total_book = $this->book_service->getTotalBookByAuthorId($author['author_id']);
		
		return view('author/detail', [
			'session' => $session,
			'author' => $author,
			'total_book' => $total_book,
			'is_logged' => $this->is_logged
		]);
	}

	public function list()
	{
		$search = $this->request->getVar('s') ?? null;
		$category = $this->request->getVar('category') ?? null;

		$result = $this->book_service->getAllBookBySearch(strtolower($search));
		$listCategory = $this->category_service->getAllCategory();

		$result_c = count($result);
		$session = session();
		return view('book/list', [
			'session' => $session,
			'search' => $search,
			'result' => $result,
			'result_c' => $result_c,
			'category' => $category,
			'listCategory' => $listCategory
		]);
	}

	public function list_filter()
	{
		$search = $this->request->getVar('s') ?? null;
		$category = $this->request->getVar('category') ?? null;

		$result = $this->book_service->getAllBookBySearchCategory(strtolower($search), $category);

		return $this->response->setJSON($result);
	}
}
