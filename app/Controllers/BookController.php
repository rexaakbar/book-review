<?php

namespace App\Controllers;

use App\Services\BookService;
use App\Services\CategoryService;
use App\Services\ReviewService;
use App\Services\AuthService;

class BookController extends BaseController
{	
	private $book_service, $category_service, $review_service, $auth_service, $is_logged;

	public function __construct(){
		$this->book_service = new BookService;
		$this->category_service = new CategoryService;
		$this->review_service = new ReviewService;
		$this->auth_service = new AuthService;
		$this->is_logged = $this->auth_service->checkLoggedUser();
	}

	public function setSessionData($message = '', $session = false, $type = 'success', $custom = false){
		$session = session();

		$session->setFlashdata('message_alert', $message);
		$session->setFlashdata('session_alert', $session);
		$session->setFlashdata('type_alert', $type);
		$session->setFlashdata('custom_alert', $custom);
	}

	public function detail($Slug = '')
	{
		$session = session();
		$user_id = $session->get('user_id');

		$book = $this->book_service->getBookBySlug($Slug);
		$review = $this->review_service->getReviewByBook($book['book_id']);
		$check_commented = $this->book_service->checkUserCommentedBook($book['book_id'], $user_id);
		
		return view('book/detail', [
			'session' => $session,
			'book' => $book,
			'review' => $review,
			'is_logged' => $this->is_logged,
			'check_commented' => $check_commented
		]);
	}

	public function list()
	{
		$search = $this->request->getVar('s') ?? null;
		$category = $this->request->getVar('category') ?? null;

		$result = $this->book_service->getAllBookBySearch(strtolower($search));
		$listCategory = $this->category_service->getAllCategory();

		$result_c = count($result);
		$session = session();
		return view('book/list', [
			'session' => $session,
			'search' => $search,
			'result' => $result,
			'result_c' => $result_c,
			'category' => $category,
			'listCategory' => $listCategory
		]);
	}

	public function list_filter()
	{
		$search = $this->request->getVar('s') ?? null;
		$category = $this->request->getVar('category') ?? null;

		$result = $this->book_service->getAllBookBySearchCategory(strtolower($search), $category);

		return $this->response->setJSON($result);
	}

	public function submit_rate(){
		try {
			$session = session();
			$rate = $this->request->getVar('rate') ?? 5;
			$comment = $this->request->getVar('comment') ?? null;
			$book_id = $this->request->getVar('book_id') ?? 0;
			$slug = $this->request->getVar('slug') ?? null;
			$user_id = $session->get('user_id');

			$res_auth = $this->book_service->submitRate($rate, $comment, $book_id, $user_id);

			if ($res_auth['state']) {
				$this->setSessionData($res_auth['message'], true, 'success');
				return redirect()->to('detail/'.$slug);
			}else{
				$this->setSessionData($res_auth['message'], true, 'warning');
				return redirect()->to('detail/'.$slug);
			}
		} catch (\Exception $e) {
			$this->setSessionData($e->getMessage(), true, 'warning');
			return redirect()->to('detail/'.$slug);
		}
	}
}
