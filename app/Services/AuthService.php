<?php

namespace App\Services;

use App\Models\UserModel;

class AuthService{
	private $user_model;

	public function __construct(){
		$this->user_model = new UserModel;
	}

	public function checkLoggedUser(){
		$session = session();
		if ($session->get('login_session')) {
			return true;
		}else{
			return false;
		}
	}

	public function login($email, $password){
		try {
			$session = session();
	        
	        $data 		= $this->user_model->where('email', $email)->first();
	        $res = [];

	        if($data){

	            $pass = $data['password'];
	            $verify_pass = password_verify($password, $pass);

	            if($verify_pass){
	                $ses_data = [
	                    'user_id'			=> $data['user_id'],
	                    'username'			=> $data['username'],
	                    'fullname'  		=> $data['fullname'],
	                    'gender'    		=> $data['gender'],
	                    'email'    			=> $data['email'],
	                    'login_session'    	=> true
	                ];
	                $session->set($ses_data);
	                
	                $res = [
	                	'state' => true,
	                	'message' => 'Login Success',
	                	'data' => $ses_data
	                ];

	            }else{
	                // $session->setFlashdata('msg', 'Wrong Password');
	                $res = [
	                	'state' => false,
	                	'message' => 'Wrong Password',
	                	'data' => []
	                ];
	            }
	        }else{
	            // $session->setFlashdata('msg', 'Email not Found');
	            $res = [
	            	'state' => false,
	            	'message' => 'Email not Found',
	            	'data' => []
	            ];
	        }
		} catch (\Exception $e) {
			$res = [
				'state' => false,
				'message' => $e->getMessage(),
				'data' => []
			];
		}

        return $res;
	}

	public function register($request){
		try {
			$data = [
			    'username'  => $request['username'],
			    'fullname' => $request['fullname'],
			    'email'    => $request['email'],
			    'password' => password_hash($request['password'], PASSWORD_DEFAULT),
			    'gender'   => $request['email'],
			];

			if ($this->user_model->save($data)) {
				$res = [
					'state' => true,
					'message' => 'Register Success',
					'data' => $data
				];
			}else{
				$res = [
					'state' => false,
					'message' => 'Register Failed',
					'data' => []
				];
			}
		} catch (\Exception $e) {
			$res = [
				'state' => false,
				'message' => $e->getMessage(),
				'data' => []
			];
		}

		return $res;
	}

	public function logout(){
		try {
			$session = session();

			if ($session->destroy()) {
				$res = [
					'state' => true,
					'message' => 'Logout Success',
					'data' => []
				];
			}else{
				$res = [
					'state' => false,
					'message' => 'Logout Failed',
					'data' => []
				];
			}
		} catch (\Exception $e) {
			$res = [
				'state' => false,
				'message' => $e->getMessage(),
				'data' => []
			];
		}
	}
}
