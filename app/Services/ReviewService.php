<?php

namespace App\Services;

use App\Models\ReviewModel;

class ReviewService{
	private $review_model;

	public function __construct(){
		$this->review_model = new ReviewModel;
	}

	public function getReviewByBook($Id){
		$data = $this->review_model->select("review.*, users.fullname as user_fullname")
		->join('users', 'users.user_id = review.user_id', 'left')
		->where(['review.book_id'=> $Id])
		->get()->getResult();
		
		return $data;
	}
	
}
