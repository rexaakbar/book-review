<?php

namespace App\Services;

use App\Models\BookModel;
use App\Models\ReviewModel;

class BookService{
	private $book_model, $review_model;

	public function __construct(){
		$this->book_model = new BookModel;
		$this->review_model = new ReviewModel;
	}

	public function getAllBook(){
		return $this->book_model->select("book.*, category.name as category_name, author.name as author_name")
			->join('category', 'category.category_id = book.category_id', 'left')
			->join('author', 'author.author_id = book.author_id', 'left')
			->get()->getResult();
	}

	public function getAllBookDashboard(){
		return $this->book_model->select("book.*, category.name as category_name, author.name as author_name")
			->join('category', 'category.category_id = book.category_id', 'left')
			->join('author', 'author.author_id = book.author_id', 'left')
			->limit(6)
			->get()->getResult();
	}

	public function getAllBookByCategory($CategoryId){
		return $this->book_model->select("book.*, category.name as category_name, author.name as author_name")
			->join('category', 'category.category_id = book.category_id', 'left')
			->join('author', 'author.author_id = book.author_id', 'left')
			->where(['book.category_id'=> $CategoryId])
			->get()->getResult();
	}

	public function getAllBookBySearch($Search = ''){
		return $this->book_model->select("book.*, category.name as category_name, author.name as author_name, 
			author.slug as author_slug, 
			(SELECT COUNT(review.review_id) as total_review FROM review WHERE review.book_id = book.book_id) total_review,
			(SELECT SUM(review.rating) as sum_rating FROM review WHERE review.book_id = book.book_id) sum_rating
			")
			->join('category', 'category.category_id = book.category_id', 'left')
			->join('author', 'author.author_id = book.author_id', 'left')
			->like('LOWER(book.title)', $Search)
			->get()->getResult();
	}

	public function getAllBookBySearchCategory($Search = '', $Category){
		return $this->book_model->select("book.*, category.name as category_name, author.name as author_name, author.slug as author_slug, 
			(SELECT COUNT(review.review_id) as total_review FROM review WHERE review.book_id = book.book_id) total_review,
			(SELECT SUM(review.rating) as sum_rating FROM review WHERE review.book_id = book.book_id) sum_rating
			")
			->join('category', 'category.category_id = book.category_id', 'left')
			->join('author', 'author.author_id = book.author_id', 'left')
			->where(['category.name'=> $Category])
			->like('LOWER(book.title)', $Search)
			->get()->getResult();
	}

	public function getBookBySlug($Slug){
		return $this->book_model->select("book.*, category.name as category_name, author.name as author_name, author.slug as author_slug, 
			(SELECT COUNT(review.review_id) as total_review FROM review WHERE review.book_id = book.book_id) total_review,
			(SELECT SUM(review.rating) as sum_rating FROM review WHERE review.book_id = book.book_id) sum_rating
			")
			->join('category', 'category.category_id = book.category_id', 'left')
			->join('author', 'author.author_id = book.author_id', 'left')
			->where('book.slug', $Slug)
			->first();
	}

	public function getTotalBookByAuthorId($Id){
		$data =  $this->book_model->select("*")
			->where('book.author_id', $Id)
			->get()->getResult();
		return count($data);
	}

	public function submitRate($rate, $comment, $book_id, $user_id){
		try {
			$data = [
			    'book_id' 		=> $book_id,
			    'user_id'    	=> $user_id,
			    'rating'    	=> $rate,
			    'review'    	=> $comment,
			    'review_date'   => date('Y-m-d H:i:s')
			];

			if ($this->review_model->insert($data)) {
				$res = [
					'state' => true,
					'message' => 'Insert Comment Success',
					'data' => $data
				];
			}else{
				$res = [
					'state' => false,
					'message' => 'Insert Comment Failed',
					'data' => $data
				];
			}
		} catch (\Exception $e) {
			$res = [
				'state' => false,
				'message' => $e->getMessage(),
				'data' => []
			];
		}

		return $res;
	}

	public function checkUserCommentedBook($book_id, $user_id){
		$data =  $this->review_model->select("*")
			->where('book_id', $book_id)
			->where('user_id', $user_id)
			->get()->getResult();
		$c = count($data);
		$res = ($c > 0) ? true : false;
		return $res;
	}
	
}
