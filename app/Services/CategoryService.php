<?php

namespace App\Services;

use App\Models\CategoryModel;

class CategoryService{
	private $category_model;

	public function __construct(){
		$this->category_model = new CategoryModel;
	}

	public function getAllCategory(){
		return $this->category_model->get()->getResult();
	}
	
}
