<?php

namespace App\Services;

use App\Models\AdminModel;

class AuthAdminService{
	private $admin_model;

	public function __construct(){
		$this->admin_model = new AdminModel;
	}

	public function login($email, $password){
		try {
			$session = session();
	        
	        $data 		= $this->admin_model->where('email', $email)->first();
	        $res = [];

	        if($data){

	            $pass = $data['password'];
	            $verify_pass = password_verify($password, $pass);

	            if($verify_pass){
	                $ses_data = [
	                    'admin_id'					=> $data['admin_id'],
	                    'admin_username'			=> $data['username'],
	                    'admin_email'    			=> $data['email'],
	                    'login_admin_session'    	=> true
	                ];
	                $session->set($ses_data);
	                
	                $res = [
	                	'state' => true,
	                	'message' => 'Login Success',
	                	'data' => $ses_data
	                ];

	            }else{
	                // $session->setFlashdata('msg', 'Wrong Password');
	                $res = [
	                	'state' => false,
	                	'message' => 'Wrong Password',
	                	'data' => []
	                ];
	            }
	        }else{
	            // $session->setFlashdata('msg', 'Email not Found');
	            $res = [
	            	'state' => false,
	            	'message' => 'Email not Found',
	            	'data' => []
	            ];
	        }
		} catch (\Exception $e) {
			$res = [
				'state' => false,
				'message' => $e->getMessage(),
				'data' => []
			];
		}

        return $res;
	}

	public function logout(){
		try {
			$session = session();

			if ($session->remove([
	                    'admin_id',
	                    'admin_username',
	                    'admin_email',
	                    'login_admin_session'
	                ])) {
				$res = [
					'state' => true,
					'message' => 'Logout Success',
					'data' => []
				];
			}else{
				$res = [
					'state' => false,
					'message' => 'Logout Failed',
					'data' => []
				];
			}
		} catch (\Exception $e) {
			$res = [
				'state' => false,
				'message' => $e->getMessage(),
				'data' => []
			];
		}
	}
}
