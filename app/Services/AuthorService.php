<?php

namespace App\Services;

use App\Models\AuthorModel;

class AuthorService{
	private $author_model;

	public function __construct(){
		$this->author_model = new AuthorModel;
	}

	public function getAllAuthor(){
		$data = $this->author_model->get()->getResult();
		
		return $data;
	}

	public function getAuthorBySlug($Slug){
		return $this->author_model->select("author.*
			")
			->where('author.slug', $Slug)
			->first();
	}
	
}
