<?php 

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;
use Config\Services;

class UserModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'user_id';
    protected $allowedFields = ['user_id', 'username', 'fullname', 'email', 'password', 'gender'];
    protected $db;

    public function __construct()
    {
        parent::__construct();
        $this->db = db_connect();
    }

}
