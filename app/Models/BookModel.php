<?php 

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;
use Config\Services;

class BookModel extends Model
{
    protected $table      = 'book';
    protected $primaryKey = 'book_id';
    protected $allowedFields = ['book_id', 'author_id', 'category_id', 'title', 'total_pages', 'book_synopsis'];
    protected $request;
    protected $db;
    protected $dt;
    protected $filtering;

    public function __construct()
    {
        parent::__construct();
        $this->db = db_connect();
    }

}
