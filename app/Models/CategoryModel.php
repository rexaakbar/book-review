<?php 

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;
use Config\Services;

class CategoryModel extends Model
{
    protected $table      = 'category';
    protected $primaryKey = 'category_id';
    protected $allowedFields = ['category_id', 'name'];
    protected $request;
    protected $db;
    protected $dt;
    protected $filtering;

    public function __construct()
    {
        parent::__construct();
        $this->db = db_connect();
    }

}
