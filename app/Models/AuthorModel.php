<?php 

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;
use Config\Services;

class AuthorModel extends Model
{
    protected $table      = 'author';
    protected $primaryKey = 'author_id';
    protected $allowedFields = ['author_id', 'name', 'alias', 'birthday_date', 'about', 'gender'];
    protected $request;
    protected $db;
    protected $dt;
    protected $filtering;

    public function __construct()
    {
        parent::__construct();
        $this->db = db_connect();
    }

}
