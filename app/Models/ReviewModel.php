<?php 

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;
use Config\Services;

class ReviewModel extends Model
{
    protected $table      = 'review';
    protected $primaryKey = 'review_id';
    protected $allowedFields = ['review_id', 'book_id', 'user_id', 'rating', 'review', 'review_date'];
    protected $request;
    protected $db;
    protected $dt;
    protected $filtering;

    public function __construct()
    {
        parent::__construct();
        $this->db = db_connect();
    }

}
