<?php 

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;
use Config\Services;

class AdminModel extends Model
{
    protected $table      = 'admin';
    protected $primaryKey = 'admin_id';
    protected $allowedFields = ['admin_id', 'username', 'email', 'password'];
    protected $db;

    public function __construct()
    {
        parent::__construct();
        $this->db = db_connect();
    }

}
