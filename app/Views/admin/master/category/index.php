<?= $this->extend('template_admin/layouts') ?>

<?= $this->section('title') ?> Master Category <?= $this->endSection() ?>

<?= $this->section('content') ?>

<div class="container-fluid px-4">
    <h1 class="mt-4">Category</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item active">Category</li>
    </ol>

    <div class="row">
        <div class="col col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <table class="table" id="tableMaster">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $j = 1; ?>
                            <?php for ($i = 0; $i < count($category); $i++) { ?>
                                <tr>
                                    <td><?= $j ?></td>
                                    <td><?= $category[$i]->name ?></td>
                                    <td><?= $category[$i]->name ?></td>
                                </tr>

                            <?php $j++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>

<?= $this->endSection() ?>

<?= $this->section('script-custom') ?>
<script type="text/javascript">
    $(document).ready(function() {
      $('#tableMaster').dataTable();
    });
</script>

<?= $this->endSection() ?>