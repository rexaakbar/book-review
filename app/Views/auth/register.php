<!DOCTYPE html>
    <head>
        <title>Book Review - Register</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Limelight&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css" rel="stylesheet" media="screen">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <div class="container-login">
            <div class="wrapper-content">
                <div class="content">
                    <a href="<?= base_url() ?>"><img src="<?= base_url() ?>/assets/logo.png" class="logo"></a>
                    <a href="<?= base_url('/') ?>" class="back-button"><span class="fa fa-arrow-left"></span> Back to home</a>
                    <div class="register-style"> Register </div>
                    <form action="<?= base_url('register_store') ?>" method="POST" accept-charset="utf-8">
                        <div class="wrapper-register">
                            <div class="wrapper-input">
                                <div class="title-input">Username</div>
                                <input type="text" placeholder="insert your username" name="username">
                                <?php if ($session->get('custom_alert')): ?>
                                    <?php if (isset($session->get('custom_alert')['username'])): ?>
                                    <div class="alert-style" id="alert-username"><?= $session->get('custom_alert')['username'] ?></div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <div class="wrapper-input">
                                <div class="title-input">Full Name</div>
                                <input type="text" placeholder="insert your full name" name="fullname">
                                <?php if ($session->get('custom_alert')): ?>
                                    <?php if (isset($session->get('custom_alert')['fullname'])): ?>
                                    <div class="alert-style" id="alert-fullname"><?= $session->get('custom_alert')['fullname'] ?></div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <div class="wrapper-input">
                                <div class="title-input">Email</div>
                                <input type="email" placeholder="insert your email" name="email">
                                <?php if ($session->get('custom_alert')): ?>
                                    <?php if (isset($session->get('custom_alert')['email'])): ?>
                                    <div class="alert-style" id="alert-email"><?= $session->get('custom_alert')['email'] ?></div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <div class="wrapper-input">
                                <div class="title-input">Password</div>
                                <input type="password" placeholder="insert your password" name="password">
                                <?php if ($session->get('custom_alert')): ?>
                                    <?php if (isset($session->get('custom_alert')['password'])): ?>
                                    <div class="alert-style" id="alert-password"><?= $session->get('custom_alert')['password'] ?></div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <div class="wrapper-input">
                                <div class="title-input">Confirm Password</div>
                                <input type="password" placeholder="insert again your password" name="confpassword">
                                <?php if ($session->get('custom_alert')): ?>
                                    <?php if (isset($session->get('custom_alert')['confpassword'])): ?>
                                    <div class="alert-style" id="alert-confpassword"><?= $session->get('custom_alert')['confpassword'] ?></div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <button class="button-register" onclick="trigger()"> Register </button>
                            <div class="wrapper-bottom">
                                <div>Have an account ?</div>
                                <a href="<?= base_url('login') ?>"><div>Click Here</div></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<script src="https://kit.fontawesome.com/104bbb7189.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.js"></script>
<script src="https://kit.fontawesome.com/104bbb7189.js" crossorigin="anonymous"></script>
<script>
    <?php if (isset($session)) { ?>
        <?php if (!is_null($session->get('session_alert'))) { ?>
            Swal.fire({
                title: '<?= $session->get('message_alert') ?>',
                icon: '<?= $session->get('type_alert') ?>',
                width: '400px',
            });
        <?php } ?>
    <?php } ?>

    function trigger() {
        console.log('asdasd')
        const username = document.getElementById('alert-username');
        const email = document.getElementById('alert-email');
        const alertPassword = document.getElementById('alert-password');
    }
</script>
<style>
body {
    display: flex;
    flex-direction: column;
}
/*  WRAPPER CONTENT*/
.container-login {
    background-color: #f9f7cf;
    height: 100vh;
    max-height: 100vh;
    overflow-y: scroll;
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */
}
.container-login::-webkit-scrollbar {
  display: none;
}
.wrapper-content {
    max-width: 800px;
    margin: 0 auto;
    padding: 0 16px;
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */
}
.wrapper-content::-webkit-scrollbar {
    display: none;
}
.wrapper-content .content {
   margin-top: 64px;
   display: flex;
   justify-content: center;
   align-items: center;
   flex-direction: column;
}
.wrapper-content .content .logo{
    width: 300px;
    height: 120px;
    margin-bottom: 32px;
}
.wrapper-content .content .back-button{
    font-size: 16px;
    margin-bottom: 16px;
    width: 400px;
    cursor: pointer;
    text-decoration: none;
    color: #4a1c40;
}
.wrapper-content .content .register-style{
    font-size: 32px;
    font-weight: 700;
    margin-bottom: 32px;
}
.wrapper-content .content .wrapper-register {
    width: 400px;
}
.wrapper-content .content .wrapper-input{
    font-size: 18px;
    margin-bottom: 16px;
}
.wrapper-content .content .wrapper-input:last-child {
    margin-bottom: 32px;
}
.wrapper-content .content .wrapper-input .title-input {
    /* color: #d99879; */
}
.wrapper-content .content .wrapper-input input{
    height: 40px;
    background-color: white;
    border: none;
    padding: 0 16px;
    border-radius: 5px;
    outline: none;
    width: 100%;
    border: 1px solid transparent;
}
.wrapper-content .content .wrapper-input input:focus{
    border: 1px solid #d99879;
}
.wrapper-content .content .button-register{
    height: 40px;
    border: 1px solid #4a1c40;
    padding: 0 16px;
    border-radius: 5px;
    background-color: transparent;
    font-weight: 600;
    color: #4a1c40;
    width: 100%;
    margin: 16px 0;
}
.wrapper-content .content .button-register:active{
    border: 1px solid #4a1c40;
    background-color: transparent;
    font-weight: 600;
    color: #fff;
    background-color: #4a1c40;
}
.wrapper-content .content .button-register:focus{
    border: 1px solid #4a1c40;
}

.wrapper-content .content .alert-style{
    color: #d84132;
}

.wrapper-content .content .show{
   display: block;
}
.wrapper-content .content .wrapper-bottom{
    display: flex;
    justify-content: space-between;
}

.wrapper-content .content .wrapper-bottom a{
    color: black;
}

@media (min-width: 768px) {
    .wrapper-content .content {
        margin-bottom: 64px;
    }
    .wrapper-content .content .wrapper-detail-book .detail-book {
        padding-left: 32px;
    }
}
@media (max-width: 768px) {
    .tabcontent {
        max-height: 400px;
        overflow-y: scroll;
    }
    .wrapper-content .bubble-comment .image{
        margin-bottom: 16px;
    }
    .wrapper-content .content .wrapper-register {
        width: 300px;
    }
}
</style>
