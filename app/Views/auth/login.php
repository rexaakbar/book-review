<!DOCTYPE html>
    <head>
        <title>Book Review - Login</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Limelight&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css" rel="stylesheet" media="screen">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <!-- <div class="wrapper-header">
            <div class="header-margin-top"></div>
            <div class="header row">
                <div class="icon-header col-md-4 col-3">ICON</div>
                <div class="search-header col-md-4 col-6">
                    <div class="wrapper-search">
                        <input type="text" placeholder="Search...">
                        <button> Search</button>
                    </div>
                </div>
                <div class="menu-header col-md-4 col-3">
                    <button class="login">Login</button>
                </div>
            </div>
        </div> -->
        <div class="container-login">
            <div class="wrapper-content">
                <div class="content">
                    <a href="<?= base_url('/') ?>"><img src="<?= base_url() ?>/assets/logo.png" class="logo"></a>
                    <a href="<?= base_url('/') ?>" class="back-button"><span class="fa fa-arrow-left"></span> Back to home</a>
                    <div class="login-style"> Login </div>
                    <form action="<?= base_url('login_store') ?>" method="POST" accept-charset="utf-8">
                        <div class="wrapper-login">
                            <div class="wrapper-input">
                                <div class="title-input">Email</div>
                                <input type="text" placeholder="insert your email" autocomplete="off" name="email">
                            </div>
                            <div class="wrapper-input">
                                <div class="title-input">Password</div>
                                <input type="password" placeholder="insert your password" name="password">
                            </div>
                            <div class="alert-style" id="alert">
                                please insert with valid email and password
                            </div>
                            <button type="submit" class="button-login" onclick="trigger()"> Login </button>
                            <div class="wrapper-bottom">
                                <div>Don't have account ?</div>
                                <a href="<?= base_url('register') ?>"><div>Click Here</div></a>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </body>
</html>
<script src="https://kit.fontawesome.com/104bbb7189.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.js"></script>
<script src="https://kit.fontawesome.com/104bbb7189.js" crossorigin="anonymous"></script>
<script>
    <?php if (isset($session)) { ?>
        <?php if (!is_null($session->get('session_alert'))) { ?>
            Swal.fire({
                title: '<?= $session->get('message_alert') ?>',
                icon: '<?= $session->get('type_alert') ?>',
                width: '400px',
            });
        <?php } ?>
    <?php } ?>

    function trigger() {
        console.log('asdasd')
        const alert = document.getElementById('alert');
        // alert.classList.add("show");
    }
</script>
<style scoped>
body {
    display: flex;
    flex-direction: column;
}
/*  WRAPPER CONTENT*/
.container-login {
    background-color: #f9f7cf;
    height: 100vh;
    max-height: 100vh;
    overflow-y: scroll;
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */
}
.container-login::-webkit-scrollbar {
  display: none;
}
.wrapper-content {
    max-width: 800px;
    margin: 0 auto;
    padding: 0 16px;
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */
}
.wrapper-content::-webkit-scrollbar {
    display: none;
}
.wrapper-content .content {
   margin-top: 64px;
   display: flex;
   justify-content: center;
   align-items: center;
   flex-direction: column;
}
.wrapper-content .content .logo{
    width: 300px;
    height: 120px;
    margin-bottom: 32px;
}
.wrapper-content .content .back-button{
    font-size: 16px;
    margin-bottom: 16px;
    width: 400px;
    cursor: pointer;
    text-decoration: none;
    color: #4a1c40;
}
.wrapper-content .content .login-style{
    font-size: 32px;
    font-weight: 700;
    margin-bottom: 32px;
}
.wrapper-content .content .wrapper-login {
    width: 400px;
}
.wrapper-content .content .wrapper-input{
    font-size: 18px;
    margin-bottom: 16px;
}
.wrapper-content .content .wrapper-input:last-child {
    margin-bottom: 32px;
}
.wrapper-content .content .wrapper-input .title-input {
    /* color: #d99879; */
}
.wrapper-content .content .wrapper-input input{
    height: 40px;
    background-color: white;
    border: none;
    padding: 0 16px;
    border-radius: 5px;
    outline: none;
    width: 100%;
    border: 1px solid transparent;
}
.wrapper-content .content .wrapper-input input:focus{
    border: 1px solid #d99879;
}
.wrapper-content .content .button-login{
    height: 40px;
    border: 1px solid #4a1c40;
    padding: 0 16px;
    border-radius: 5px;
    background-color: transparent;
    font-weight: 600;
    color: #4a1c40;
    width: 100%;
    margin: 16px 0;
}
.wrapper-content .content .button-login:active{
    border: 1px solid #4a1c40;
    background-color: transparent;
    font-weight: 600;
    color: #fff;
    background-color: #4a1c40;
}
.wrapper-content .content .button-login:focus{
    border: 1px solid #4a1c40;
}

.wrapper-content .content .alert-style{
    color: #d84132;
    display: none;
}

.wrapper-content .content .show{
   display: block;
}

.wrapper-content .content .wrapper-bottom{
    display: flex;
    justify-content: space-between;
}

.wrapper-content .content .wrapper-bottom a{
    color: black;
}

@media (min-width: 768px) {
    .wrapper-content .content {
        margin-bottom: 64px;
    }
    .wrapper-content .content .wrapper-detail-book .detail-book {
        padding-left: 32px;
    }
}
@media (max-width: 768px) {
    .tabcontent {
        max-height: 400px;
        overflow-y: scroll;
    }
    .wrapper-content .bubble-comment .image{
        margin-bottom: 16px;
    }
    .wrapper-content .content .wrapper-login {
        width: 300px;
    }
}
</style>
