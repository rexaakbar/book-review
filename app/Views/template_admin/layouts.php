<!DOCTYPE html>
<html>
	<head>
		<!-- Title -->
		<title>Book Review Admin - <?= $this->renderSection('title') ?></title>
		<!-- Head -->
		<?= $this->include('template_admin/header') ?>	
		<!-- Stylesheet Css -->
		<?= $this->include('template_admin/stylesheet') ?>

		<!-- Stylesheet Css Custom -->
		<?= $this->renderSection('stylesheet-custom') ?>
	</head>
	<body class="sb-nav-fixed" data-url="<?= base_url() ?>">
		<!-- Nav Bar Header -->
		<?= $this->include('template_admin/head') ?>

		<div id="layoutSidenav">
		    <div id="layoutSidenav_nav">
		        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
		            <div class="sb-sidenav-menu">
		                <div class="nav">
		                    <div class="sb-sidenav-menu-heading">Master</div>
		                    <a class="nav-link" href="<?= base_url() ?>/admin/master/category">
		                        <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
		                        Category
		                    </a>
		                </div>
		            </div>
		            <div class="sb-sidenav-footer">
		            </div>
		        </nav>
		    </div>
		    <div id="layoutSidenav_content">
		        <main>
		        	<!-- Content Section -->
		        	<?= $this->renderSection('content') ?>
		            
		        </main>
		        <footer class="py-4 bg-light mt-auto">
		            <div class="container-fluid px-4">
		                <div class="d-flex align-items-center justify-content-between small">
		                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
		                    <div>
		                        <a href="#">Privacy Policy</a>
		                        &middot;
		                        <a href="#">Terms &amp; Conditions</a>
		                    </div>
		                </div>
		            </div>
		        </footer>
		    </div>
		</div>

		<!-- Script JS -->
		<?= $this->include('template_admin/script') ?>

		<!-- Script JS Custom -->
		<?= $this->renderSection('script-custom') ?>
	</body>
</html>