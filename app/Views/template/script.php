<script src="https://kit.fontawesome.com/104bbb7189.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.js"></script>
<script src="https://kit.fontawesome.com/104bbb7189.js" crossorigin="anonymous"></script>
<script>

    <?php if (isset($session)) { ?>
        <?php if (!is_null($session->get('session_alert'))) { ?>
            Swal.fire({
                title: '<?= $session->get('message_alert') ?>',
                icon: '<?= $session->get('type_alert') ?>',
                width: '400px',
            });
        <?php } ?>
    <?php } ?>

    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = 40;
    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();
        
        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;
        console.log("document.getElementsByClassName", document.getElementsByClassName('header'))
        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('#wrapper-header').addClass('hide-header');
            $('#wrapper-header').removeClass('show-header');
            console.log("IF", document.getElementById('wrapper-header').className)
        } else {
            // Scroll Up.
            console.log("ELSE", document.getElementById('wrapper-header').className)
            if(st + $(window).height() < $(document).height()) {
                $('#wrapper-header').addClass('show-header');
                $('#wrapper-header').removeClass('hide-header');
            }
        }
        
        lastScrollTop = st;
    }
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
</script>