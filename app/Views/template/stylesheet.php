<style>
body {
    display: flex;
    flex-direction: column;
}

/*  WRAPPER  HEADER*/

.wrapper-header {
    background-color: #f9f7cf;
    height: 100px;
    width: 100%;
    position: fixed;
    z-index: 100;
    top: 0;
    transition: top 0.2s ease-in-out;
}
.wrapper-header.hide-header {
    top: -100px;
}
.wrapper-header .header-margin-top {
    margin-top: 30px;
}
.wrapper-header .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    max-width: 1080px;
    margin: 0 auto;
}
.wrapper-header .header .search-header .wrapper-search {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: 40px;
}
.wrapper-header .header .search-header .wrapper-search input, .wrapper-header .header .search-header .wrapper-search input:focus {
    width: 100%;
    height: 40px;
    background-color: white;
    border: none;
    padding: 0 16px;
    border-radius: 5px 0 0 5px;
    outline: none;
}
.wrapper-header .header .search-header .wrapper-search button {
    height: 40px;
    border: 1px solid #4a1c40;
    padding: 0 16px;
    border-radius: 0 5px 5px 0;
    background-color: transparent;
    font-weight: 600;
    color: #4a1c40;
}
.wrapper-header .header .search-header .wrapper-search button:hover, .wrapper-header .header .search-header .wrapper-search button:focus {
    border: 1px solid #bbbbbb;
    background-color: #4a1c40;
    outline: none;
    color: #fff;
}
.wrapper-header .header .menu-header {
    width: 33%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    height: 40px;
}
.wrapper-header .header .menu-header .login {
    height: 40px;
    border: 1px solid #4a1c40;
    padding: 0 16px;
    border-radius: 5px;
    background-color: transparent;
    font-weight: 600;
    color: #4a1c40;
}
.wrapper-header .header .menu-header .login:hover, .wrapper-header .header .menu-header .login:focus {
    border: 1px solid #bbbbbb;
    background-color: #4a1c40;
    outline: none;
    color: #fff;
}
.wrapper-header .header .icon-header img{
    height: 48px;
    cursor: pointer;
}

</style>
<link href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css" rel="stylesheet" media="screen">