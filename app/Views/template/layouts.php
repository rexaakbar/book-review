<!DOCTYPE html>
<html>
	<head>
		<!-- Title -->
		<title>Book Review - <?= $this->renderSection('title') ?></title>
		<!-- Head -->
		<?= $this->include('template/header') ?>	
		<!-- Stylesheet Css -->
		<?= $this->include('template/stylesheet') ?>

		<!-- Stylesheet Css Custom -->
		<?= $this->renderSection('stylesheet-custom') ?>
	</head>
	<body class="hold-transition sidebar-mini layout-fixed" data-url="<?= base_url() ?>">
		<!-- Nav Bar Header -->
		<?= $this->include('template/head') ?>

		<!-- Content Section -->
		<?= $this->renderSection('content') ?>

		<!-- Script JS -->
		<?= $this->include('template/script') ?>

		<!-- Script JS Custom -->
		<?= $this->renderSection('script-custom') ?>
        <div id="loading" style="display: none">
            <i id="spinner" class="fa fa-cog fa-spin fa-5x fa-fw"></i>
        </div>
	</body>
	<!-- Footer -->
	<footer class="main-footer">
		<?= $this->include('template/footer') ?>
	</footer>
</html>