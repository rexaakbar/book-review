<div class="wrapper-header show-header" id="wrapper-header">
    <div class="header-margin-top"></div>
    <div class="header row">
        <div class="icon-header col-md-4 col-3 p-0">
            <a href="<?= base_url() ?>"><img src="<?= base_url() ?>/assets/logo.png"></a>
        </div>
        <div class="search-header col-md-4 col-6 p-0">
            <form action="<?= base_url('book/list') ?>" method="GET" accept-charset="utf-8">
                <div class="wrapper-search">
                    <input type="text" placeholder="Search..." name="s">
                    <button type="submit"> <span class="fa fa-search"></span></button>
                </div>
            </form>
        </div>
        <div class="menu-header col-md-4 col-3">
            <?php if ($session->get('login_session')) { ?>
                <span class="mr-2">Hi, <?= $session->get('fullname') ?> </span>
                <a href="<?= base_url('logout_store') ?>"><button class="login">Logout</button></a>
            <?php }else{ ?>
                <a href="<?= base_url('login') ?>"><button class="login">Login</button></a>
            <?php } ?>
        </div>
    </div>
</div>