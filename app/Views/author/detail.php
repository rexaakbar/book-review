<?= $this->extend('template/layouts') ?>

<?= $this->section('title') ?> Author Detail <?= $this->endSection() ?>

<?= $this->section('content') ?>

<div class="wrapper-content">
    <div class="content">
        <div class="wrapper-detail-book row">
            <div class="image-author col-12 col-md-3">
                <img src="<?= base_url() ?>/assets/author1.jpg">
            </div>
            <div class="detail-book col-12 col-md-9">
                <div class="title-book"><?= $author['name'] ?></div>
                
                <div class="description-content">
                    <div class="tab">
                        <button class="tablinks active" onclick="openTabs(event, 'Description')" id="defaultOpen">Description</button>
                    </div>
                    <div class="wrapper-tabs">
                        <div id="Description" class="tabcontent">
                            <!-- <p> Institution: McKinsey & Company</p> -->
                            <?php if ($author['birthday_date'] != "") { ?>
                            <p> Birthdate : <? $author['birthday_date'] ?></p>
                            <?php } ?>
                            <!-- <p>Country: Netherlands</p> -->
                            <p>Number of Books: <?= $total_book ?></p>
                            <?php if ($author['birthday_date'] != "") { ?>
                            <p>Contact the Author:</p>
                            <?php } ?>
                            <!-- <p>PROF. DR. NICK VAN DAM is full professor CORPORATE LEARNING & LEADERSHIP DEVELOPMENT.</p> -->
                            
                            <p><?= $author['about'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        

<style>
body {
    display: flex;
    flex-direction: column;
}
/*  WRAPPER CONTENT*/
.wrapper-content {
    max-width: 1080px;
    margin: 0 auto 100px;
    padding: 0 16px;
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */
}
.wrapper-content::-webkit-scrollbar {
    display: none;
}
.wrapper-content .content {
   margin-top: 100px;
   padding-top: 64px;
}
.wrapper-content .content .wrapper-detail-book {
    display: flex;
    margin: 0;
}
.wrapper-content .content .wrapper-detail-book .image-author {
    display: flex;
    justify-content: center;
    margin-bottom: 16px;
    padding: 0;
    height: 390px;
}
.wrapper-content .content .wrapper-detail-book .image-author img {
    height: 200px;
    width: 200px;
    object-fit: cover;
    border-radius: 50%;
}
.wrapper-content .content .wrapper-detail-book .detail-book {
    display: flex;
    flex-direction: column;
    padding: 0;
}
.wrapper-content .content .wrapper-detail-book .detail-book .title-book {
    font-size: 32px;
}
.wrapper-content .content .wrapper-detail-book .detail-book .author-book {
    font-size: 16px;
    margin-bottom: 8px;
}
.wrapper-content .content .wrapper-detail-book .detail-book .author-book .author-name{
    color: #d99879;
    cursor: pointer;
    font-weight: 600;
}
.wrapper-content .content .wrapper-detail-book .detail-book .author-book .author-name:hover{
    text-decoration: underline;
}
.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info{
    display: flex;
    margin-bottom: 32px;
}
.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info .rating{
    display: flex;
    justify-content: space-between;
}
.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info .rating .fa-star{
    font-size: 18px;
    color: #f9b208;
}
.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info .info-book {
    font-weight: 600;
}
.wrapper-content .content .wrapper-detail-book .detail-book .description-content {
    margin-bottom: 32px;
}
/* TABS */
.tab {
    overflow: hidden;
    padding: 0 8px;
    margin-bottom: -2px;
    z-index: 2;
}
.tab button {
    color: #495057;
    border: none;
    background-color: transparent;
    padding: 0 8px;
    border-radius: 3px 3px 0 0;
    height: 32px;
}
.tab button.active{
    border-bottom: 3px solid #4a1c40;
    background-color: transparent;
    color: #495057;
    font-weight: 600;
    border: none;
}
.tab button:active{
    color: #495057;
}
.tab button:focus{
    outline: none;
}
.wrapper-tabs {
    width: 100%;
}
.tabcontent {
  /*display: none;*/
  padding: 8px 16px;
  border: 1px solid #d99879;
  border-radius: 5px;
}
@media (min-width: 768px) {
    .wrapper-content .content {
        margin-bottom: 64px;
    }
    .wrapper-content .content .wrapper-detail-book .detail-book {
        padding-left: 32px;
    }
}
@media (max-width: 768px) {
    .tabcontent {
        max-height: 400px;
        overflow-y: scroll;
    }
    .wrapper-content .bubble-comment .image{
        margin-bottom: 16px;
    }
}
</style>

<?= $this->endSection() ?>

<?= $this->section('script-custom') ?>

<?= $this->endSection() ?>