<?= $this->extend('template/layouts') ?>

<?= $this->section('title') ?> Home <?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="slider">
    <img src="<?= base_url() ?>/assets/background.jpg">
</div> 
<div class="wrapper-content">
    <div class="content">
        <div class="motivation row justify-content-between">
            <div class="motivation-detail col-md-6 col-12">
                <blockquote class="wrapper-quote">
                    <span class="double-quote">&#8220;</span>
                    <div class="quote">
                        Pada akhirnya nanti, semua yang pernah hilang atau diambil dari diri kita akan kembali lagi kepada kita. Walaupun dengan cara yang tidak pernah kita duga. 
                    </div>
                    <div class="from-book">Harry Potter and the Order of the Phoenix, J.K. Rowling</div>
                </blockquote>
            </div>  
            <div class="motivation-detail col-md-6 col-12">
                <blockquote class="wrapper-quote">
                    <span class="double-quote">&#8220;</span>
                    <div class="quote">
                        Cinta akan membuatmu ingin menjadi orang yang lebih baik; benar, benar. Tapi mungkin cinta, cinta sejati juga memberimu kesempatan untuk menjadi dirimu sendiri. 
                    </div>
                    <div class="from-book">Gone Girl, Gillian Flynn</div>
                </blockquote>
            </div>
        </div>
        <div class="title-categories">
            Book Display
        </div>
        <div>
            <div class="seemore"><a href="<?= base_url('book/list') ?>">See More</a></div>
            <div class="categories row">
                <?php for ($i = 0; $i < count($book); $i++) { ?>
                    <div class="display-book col-sm-12 col-md-2">
                        <a href="<?= base_url('/book/detail') . '/' . $book[$i]->slug ?>"><img src="<?= base_url() ?>/assets/<?= $book[$i]->image ?>"></a>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="highlight-review">
            <div class="testimonial-text">Testimonial</div>
            <div class="row">
                <div class="review col-md-6">
                    <img class="image-reviewer" src="<?= base_url() ?>/assets/guy1.jpg">
                    <div class="content-reviewer">
                        <div class="text-reviewer">Nice Website, very recommended for searching a good quality book.</div>
                        <div class="name-reviewer"> Nick van Dam</div>
                    </div>
                </div>
                <div class="review col-md-6">
                    <img class="image-reviewer" src="<?= base_url() ?>/assets/guy2.jpg">
                    <div class="content-reviewer">
                        <div class="text-reviewer">Simple but functional. </div>
                        <div class="name-reviewer"> Grace D'e Nae</div>
                    </div>
                </div>
                <div class="review col-md-6">
                    <img class="image-reviewer" src="<?= base_url() ?>/assets/guy3.jpg">
                    <div class="content-reviewer">
                        <div class="text-reviewer">This website help me so much for searching a good quality book. Thank you.</div>
                        <div class="name-reviewer"> Nai Ne</div>
                    </div>
                </div>
                <div class="review col-md-6">
                    <img class="image-reviewer" src="<?= base_url() ?>/assets/guy4.jpg">
                    <div class="content-reviewer">
                        <div class="text-reviewer">Nice Website, very recommended for searching good quality book</div>
                        <div class="name-reviewer"> Angel Jesicca</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style scoped>
body {
    display: flex;
    flex-direction: column;
}

/*  WRAPPER CONTENT*/
.slider {
    height: 500px;
    width: 100%;
    background-color: #f9dfdc;
    margin: 100px auto 32px auto;
    display: flex;
    justify-content: center;
}
.slider img{
    height: inherit;
    width: inherit;
    background-color: #f9dfdc;
    object-fit: cover;
}
.wrapper-content {
    max-width: 1080px;
    margin: 0 auto 100px;
    padding: 0 16px;
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */
}
.wrapper-content::-webkit-scrollbar {
    display: none;
}
.wrapper-content .content {
   /* margin-top: 100px; */
}
.wrapper-content .content .motivation {
    display: flex;
    margin: 0;
    margin-bottom: 24px;
}
@media (min-width: 768px) {
    .wrapper-content .content .motivation .motivation-detail {
        max-width: 50%;
        flex: 0 0 49%;
    }
}
.wrapper-content .content .motivation .motivation-detail {
    padding: 0;
    margin-bottom: 8px;
}
.wrapper-content .content .motivation .motivation-detail .wrapper-quote {
    background-color: #feddbe;
    padding: 16px;
    border-radius: 5px;
}
.wrapper-content .content .motivation .motivation-detail .wrapper-quote .double-quote {
    font-size: 40px;
    font-family: 'Limelight', cursive;
    line-height: normal;
    position: absolute;
    margin-top: -2px;
    color: rgba(0,0,0,.54);
}
.wrapper-content .content .motivation .motivation-detail .wrapper-quote .quote {
    font-size: 16px;
    line-height: normal;
    padding-left: 32px;
    text-align: justify;
    font-style: italic;
}
.wrapper-content .content .motivation .motivation-detail .wrapper-quote .from-book {
    display: block;
    color: rgba(0,0,0,.54);
    font-weight: 700;
    font-size: 14px;
    font-style: normal;
    margin-top: 10px;
    padding-left: 32px;
}
.wrapper-content .content .motivation .motivation-detail .quote::-webkit-scrollbar {
    display: none;
}
.wrapper-content .content .title-categories {
    width: 100%;
    height: 80px;
    background-color: #d99879;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 32px;
    font-weight: 600;
    color: #fff;
    margin-bottom: 32px;
    border-radius: 5px;
}
.wrapper-content .content .categories {
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin: 0;
    margin-bottom: 32px;
}
.wrapper-content .content .categories .display-book {
    height: 200px;
    display: flex;
    justify-content: center;
    cursor: pointer;
}
.wrapper-content .content .categories .display-book a {
    height: inherit;
    object-fit: contain;
    width: inherit;
}
.wrapper-content .content .categories .display-book img{
    height: inherit;
    object-fit: cover;
    width: inherit;
}
.wrapper-content .content .highlight-review{
    width: 100%;
    padding: 16px 32px 32px 32px;
    border: 1px solid #f2dac3;
    margin: 0;
    border-radius: 5px;
}
.wrapper-content .content .highlight-review .testimonial-text{
    text-align: center;
    margin-bottom: 32px;
    font-size: 32px;
    font-weight: 600;
}
.wrapper-content .content .highlight-review .review{
    width: 100%;
    display: flex;
    margin-bottom: 0;
    align-items: center;
}
.wrapper-content .content .highlight-review .review:nth-child(-n+2){
    margin-bottom: 16px;
}
.wrapper-content .content .highlight-review .review .image-reviewer{
    margin-right: 16px;
    height: 120px;
    width: 120px;
    border-radius: 50%;
    object-fit: cover;
}
.wrapper-content .content .highlight-review .review .content-reviewer .text-reviewer{
    margin-bottom: 8px;
}
.wrapper-content .content .highlight-review .review .content-reviewer .text-reviewer::-webkit-scrollbar {
    display: none;
}
.wrapper-content .content .highlight-review .review .content-reviewer .name-reviewer{
    font-size: 13px;
    font-weight: 700;
}
.wrapper-content .seemore {
    padding-right: 16px;
    margin-bottom: 16px;
    text-align: right;
}
.wrapper-content .seemore a{
    color: #d99879;
    cursor: pointer;
}
</style>

<?= $this->endSection() ?>

<?= $this->section('script-custom') ?>

<?= $this->endSection() ?>