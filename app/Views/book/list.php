<?= $this->extend('template/layouts') ?>

<?= $this->section('title') ?> Book List <?= $this->endSection() ?>

<?= $this->section('content') ?>

<div class="wrapper-content">
    <div class="content">
        <div class="wrap-book-list">
            <div class="filter">
                <div class="title-filter" onclick="showFilter()">Filter <span class="fa fa-caret-up" id="icon-caret"></span></div>
                <div class="filter-list mt-2 show" id="filter" >
                    <!-- <div class="list" onclick="selectedCategorie(event, 'Economy')" id="Economy">Economy</div>
                    <div class="list" onclick="selectedCategorie(event, 'Geography')" id="Geography">Geography</div>
                    <div class="list" onclick="selectedCategorie(event, 'Fantasy')" id="Fantasy">Fantasy</div>
                    <div class="list" onclick="selectedCategorie(event, 'Engineering')" id="Engineering">Engineering</div>
                    <div class="list" onclick="selectedCategorie(event, 'Management')" id="Management">Management</div>
                    <div class="list" onclick="selectedCategorie(event, 'Communitcation')" id="Communitcation">Communitcation & persentation</div>
                    <div class="list" onclick="selectedCategorie(event, 'Economic')" id="Economic">Economic & Finance</div>
                    <div class="list" onclick="selectedCategorie(event, 'Personal')" id="Personal">Personal Development</div>
                    <div class="list" onclick="selectedCategorie(event, 'Digital')" id="Digital">Digital</div>
                    <div class="list" onclick="selectedCategorie(event, 'Camera')" id="Camera">Camera</div>
                    <div class="list" onclick="selectedCategorie(event, 'IT')" id="IT">IT</div> -->
                    <?php for ($i = 0; $i < count($listCategory); $i++) { ?>
                        <div class="list" onclick="selectedCategorie(event, '<?= $listCategory[$i]->name ?>')" id="<?= $listCategory[$i]->name ?>"><?= $listCategory[$i]->name ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="w-100">
                <?php if ($search != "") { ?>
                    <div class="mb-3">
                        <div>Search results for "<b><?= $search ?></b>"</div>
                        <div id="resultFound">We found <?= $result_c ?> results</div>
                    </div>
                <?php } ?>
                <div class="w-100 row m-0" id="container_list_book">
                <?php if ($result_c > 0) { ?>
                    <?php for ($i = 0; $i < $result_c; $i++) { ?>
                        <a href="<?= base_url() . '/book/detail/' . $result[$i]->slug ?>">
                            <div class="list-book col-md-3 col-6">
                                <div class="book border-shape">
                                    <img class="border-shape image-style" src="<?= base_url() ?>/assets/<?= $result[$i]->image ?>">
                                    <a href="<?= base_url() . '/author/detail/' . $result[$i]->author_slug ?>"><div class="mt-2 author-name"><?= $result[$i]->author_name ?></div></a>
                                    <div class="mb-1 f-13"><?= $result[$i]->title ?></div>
                                    <div class="star">
                                        <span class="fa fa-star">
                                        </span>
                                        <div class="mr-2"> <?= ($result[$i]->sum_rating != 0) ? (float) ($result[$i]->sum_rating / $result[$i]->total_review) : 0 ?></div>
                                        <i> (<?= $result[$i]->total_review ?> review) </i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                <?php } else { ?>
                    <div class="text-center">No Found Book</div>
                <?php } ?>
                    <!-- <div class="list-book col-md-3 col-6">
                        <div class="book border-shape">
                            <img class="border-shape image-style" src="<?= base_url() ?>/assets/book1.jpg">
                            <div class="mt-2 author-name">Dr. Joe Martelli</div>
                            <div class="mb-1 f-13">Nama Bukunya Harusnya</div>
                            <div class="star">
                                <span class="fa fa-star">
                                </span>
                                <div class="mr-2"> 4.5 </div>
                                <i> (18 review) </i>
                            </div>
                        </div>
                    </div>
                    <div class="list-book col-md-3 col-6">
                        <div class="book border-shape">
                            <img class="border-shape image-style" src="<?= base_url() ?>/assets/book2.jpg">
                            <div class="mt-2 author-name">Dr. Joe Martelli</div>
                            <div class="mb-1 f-13">Nama Bukunya Harusnya</div>
                            <div class="star">
                                <span class="fa fa-star">
                                </span>
                                <div class="mr-2"> 4.5 </div>
                                <i> (18 review) </i>
                            </div>
                        </div>
                    </div>
                    <div class="list-book col-md-3 col-6">
                        <div class="book border-shape">
                            <img class="border-shape image-style" src="<?= base_url() ?>/assets/book3.jpg">
                            <div class="mt-2 author-name">Dr. Joe Martelli</div>
                            <div class="mb-1 f-13">Nama Bukunya Harusnya</div>
                            <div class="star">
                                <span class="fa fa-star">
                                </span>
                                <div class="mr-2"> 4.5 </div>
                                <i> (18 review) </i>
                            </div>
                        </div>
                    </div>
                    <div class="list-book col-md-3 col-6">
                        <div class="book border-shape">
                            <img class="border-shape image-style" src="<?= base_url() ?>/assets/book4.jpg">
                            <div class="mt-2 author-name">Dr. Joe Martelli</div>
                            <div class="mb-1 f-13">Nama Bukunya Harusnya</div>
                            <div class="star">
                                <span class="fa fa-star">
                                </span>
                                <div class="mr-2"> 4.5 </div>
                                <i> (18 review) </i>
                            </div>
                        </div>
                    </div>
                    <div class="list-book col-md-3 col-6">
                        <div class="book border-shape">
                            <img class="border-shape image-style" src="<?= base_url() ?>/assets/book5.jpg">
                            <div class="mt-2 author-name">Dr. Joe Martelli</div>
                            <div class="mb-1 f-13">Nama Bukunya Harusnya</div>
                            <div class="star">
                                <span class="fa fa-star">
                                </span>
                                <div class="mr-2"> 4.5 </div>
                                <i> (18 review) </i>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<style scoped>
body {
    display: flex;
    flex-direction: column;
}

/*  WRAPPER CONTENT*/
#container_list_book {
    min-height: 285px;
}
.wrapper-content {
    max-width: 1080px;
    margin: 0 auto 100px;
    padding: 0 16px;
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */
}
.wrapper-content::-webkit-scrollbar {
    display: none;
}
.wrapper-content .content {
    margin-top: 100px;
    padding-top: 64px;
    text-align: left;
    max-width: 1080px;
    min-width: 1080px;
}

.wrapper-content .content .wrap-book-list {
    display: flex;
}

.wrapper-content .content .wrap-book-list .filter{
   width: 18%;
   height: 100%;
   padding: 8px;
   border: 1px solid #d99879;
   border-radius: 5px;
   margin-right: 32px;
}
.wrapper-content .content .wrap-book-list .filter .title-filter{
   font-size: 20px;
   font-weight: 700;
   display: flex;
   justify-content: space-between;
   user-select: none;
   cursor: pointer;
   align-items: center;
   padding: 0 8px;
}
.wrapper-content .content .wrap-book-list .filter .filter-list .list{
    padding: 4px 8px;
    cursor: pointer;
    user-select: none;
}
.wrapper-content .content .wrap-book-list .filter .filter-list .list.active{
    background-color: #d99879;
    border-radius: 5px;
    color: #fff;
}
.wrapper-content .content .wrap-book-list .filter .filter-list.hide{
    display: none;
}
.wrapper-content .content .wrap-book-list .filter .filter-list .list:hover{
    background-color: #d99879;
    border-radius: 5px;
    color: #fff;
}
.wrapper-content .content .wrap-book-list .list-book{
    display: flex;
    padding: 0;
}
.wrapper-content .content .wrap-book-list .list-book .book{
    padding: 8px;
    cursor: pointer;
    user-select: none;
}
.wrapper-content .content .wrap-book-list .list-book .book:hover{
    background-color: #f9f7cf;
}
.wrapper-content .content .wrap-book-list .list-book .book .image-style{
    object-fit: contain;
    width: 100%;
    height: 200px;
}
.wrapper-content .content .wrap-book-list .list-book .book .author-name{
    color: #d99879;
    cursor: pointer;
    font-weight: 600;
    font-size: 11px;
    user-select: none;
}
.wrapper-content .content .wrap-book-list .list-book .book .author-name:hover{
    text-decoration: underline;
}
.wrapper-content .content .wrap-book-list .list-book .book .star {
    display: flex;
    align-items: center;
    font-size: 15px;
}
.wrapper-content .content .wrap-book-list .list-book .book .fa-star{
    color: #f9b208;
    margin-right: 8px;
}
.f-13 {
    font-size: 13px;
}
.border-shape {
    border-radius: 5px;
}
/* TABS */
.tab {
    overflow: hidden;
    padding: 0 8px;
    margin-bottom: -2px;
    z-index: 2;
}
.tab button {
    color: #495057;
    border: none;
    background-color: transparent;
    padding: 0 8px;
    border-radius: 3px 3px 0 0;
    height: 32px;
}
.tab button.active{
    border-bottom: 3px solid #4a1c40;
    background-color: transparent;
    color: #495057;
    font-weight: 600;
}
.tab button:active{
    color: #495057;
}
.wrapper-tabs {
    width: 100%;
}
@media (min-width: 768px) {
    .wrapper-content .content {
        margin-bottom: 64px;
    }
    .wrapper-content .content .wrapper-detail-book .detail-book {
        padding-left: 32px;
    }
}
@media (max-width: 768px) {
    .wrapper-content .content {
        padding-top: 32px;
        min-width: 100%;
        min-height: 100vh;
    }
    .wrapper-content .bubble-comment .image{
        margin-bottom: 16px;
    }
    .wrapper-content .content .wrap-book-list {
        display: block;
    }
    .wrapper-content .content .wrap-book-list .filter{
        max-width: 286px;
        width: 100%;
        margin-right: 0;
        margin-bottom: 32px;
    }
    .wrapper-content .content .wrap-book-list .filter .title-filter{
        font-size: 16px;
    }
    .wrapper-content .content .wrap-book-list .filter .filter-list{
        width: 100%;
        margin: 0;
    }
    .wrapper-content .content .wrap-book-list .filter .filter-list .list{
        width: 100%;
    }
}
</style>

<?= $this->endSection() ?>

<?= $this->section('script-custom') ?>

<script>
    // Hide Header on on scroll down
    var isShowFilter = true;
    function selectedCategorie(evt, selectedTabs) {
        var i, listCategory;
        listCategory = document.getElementsByClassName("list");
        for (i = 0; i < listCategory.length; i++) {
            listCategory[i].className = listCategory[i].className.replace(" active", "");
        }
        document.getElementById(selectedTabs).style.display = "block";
        evt.currentTarget.className += " active";

        // $('#container_list_book').html('')

        $.ajax({
          url : "<?= base_url('book/list_filter') ?>",
          type: 'get',
          data: {
              's': '<?= $search ?>',
              'category': selectedTabs,
          },
          dataType: 'json',
          success: (response) => {
            var resultHtml = ''
            $('#resultFound').html("We found " + response.length + " results")

            $.each(response, function(element, index) {
                resultHtml += '<a href="<?= base_url() . '/book/detail/'?>'+index.slug+'">'
                resultHtml += '<div class="list-book col-md-3 col-6">'
                resultHtml += '<div class="book border-shape">'
                resultHtml += '<img class="border-shape image-style" src="<?= base_url() ?>/assets/'+index.image+'">'
                resultHtml += '<a href="<?= base_url() . '/author/detail/'?>'+index.author_slug+'"><div class="mt-2 author-name">' + index.author_name + '</div></a>'
                resultHtml += '<div class="mb-1 f-13">' + index.title + '</div>'
                resultHtml += '<div class="star">'
                resultHtml += '<span class="fa fa-star">'
                resultHtml += '</span>'
                resultHtml += '<div class="mr-2"> ' + ((index.sum_rating != null) ? parseFloat((index.sum_rating / index.total_review)) : 0) + '</div>'
                resultHtml += '<i> (' + index.total_review + ' review) </i>'
                resultHtml += '</div>'
                resultHtml += '</div>'
                resultHtml += '</div>'
                resultHtml += '</a>'
            });

            $('#container_list_book').html(resultHtml)
          }
        })
    }
    function showFilter() {
        isShowFilter = !isShowFilter
        if (isShowFilter) {
            $('#filter').addClass('show').removeClass('hide');
            $('#icon-caret').removeClass('fa-caret-down').addClass('fa-caret-up');
        } else {
            console.log()
            $('#filter').addClass('hide').removeClass('show');
            $('#icon-caret').removeClass('fa-caret-up').addClass('fa-caret-down');
        }
    }
    window.showFilter();
</script>

<?= $this->endSection() ?>