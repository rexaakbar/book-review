<?= $this->extend('template/layouts') ?>

<?= $this->section('title') ?> Book Detail <?= $this->endSection() ?>

<?= $this->section('content') ?>

<div class="wrapper-content">
    <div class="content">
        <div class="wrapper-detail-book row">
            <div class="image-book col-12 col-md-3">
                <img src="<?= base_url() ?>/assets/<?= $book['image'] ?>"">
            </div>
            <div class="detail-book col-12 col-md-9">
                <div class="title-book"> <?= $book['title'] ?></div>
                <div class="author-book"> by <a href="<?= base_url() ?>/author/detail/<?= $book['author_slug'] ?>"><span class="author-name"><?= $book['author_name'] ?></span></a></div>
                <div class="wrapper-detail-book-info">
                    <div class="rating mr-4">
                        <div class="mr-2">Rating: </div>
                        <div class="mr-2">
                            <?php 
                                $floor = (float) ($book['sum_rating'] > 0 ) ? floor($book['sum_rating'] / $book['total_review']) : 0;
                                $real = (float) ($book['sum_rating'] > 0 ) ? ($book['sum_rating'] / $book['total_review']) : 0;
                                $real_ = $floor;
                                $real = $real - $floor;
                             ?>
                            <?php for ($i = 0; $i < $real_; $i++) { ?>
                                <span class="fa fa-star">
                                </span>
                            <?php } ?>

                             <?php if ($real >= 0.5): ?>
                                 <span class="fa fa-star-half">
                                 </span>
                             <?php endif ?>
                        </div>
                        <div class="mr-2"> <?= (float) ($book['sum_rating'] > 0 ) ? ($book['sum_rating'] / $book['total_review']) : 0?></div>
                        <div>(<?= $book['total_review'] ?> Review)</div>
                    </div>
                    <div class="info-book">
                        <div class="count-page"><?= $book['total_pages'] ?> Pages</div>
                    </div>
                </div>
                <div class="description-content">
                    <div class="tab">
                        <button class="tablinks" onclick="openTabs(event, 'Description')" id="defaultOpen">Description</button>
                        <button class="tablinks" onclick="openTabs(event, 'Content')">Content</button>
                    </div>
                    <div class="wrapper-tabs">
                        <div id="Description" class="tabcontent">
                            <p><?= $book['description'] ?></p>
                        </div>
                        <div id="Content" class="tabcontent">
                            <p><?= $book['book_synopsis'] ?></p> 
                        </div>
                    </div>
                </div>
                <!-- <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link active" id="nav-description-tab" data-toggle="tab" href="#nav-description" role="tab" aria-controls="nav-description" aria-selected="true">Description</a>
                      <a class="nav-item nav-link" id="nav-content-tab" data-toggle="tab" href="#nav-content" role="tab" aria-controls="nav-content" aria-selected="false">Content</a>
                    </div>
                </nav> -->
                <!-- <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-description" role="tabpanel" aria-labelledby="nav-description-tab">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has 
                    </div>
                    <div class="tab-pane fade" id="nav-content" role="tabpanel" aria-labelledby="nav-profile-tab">asdsadasdsa</div>
                </div> -->
            </div>
        </div>
    </div>
    <?php if ($is_logged && !$check_commented): ?>
        <form action="<?= base_url('book/submit_rate') ?>" method="POST" accept-charset="utf-8">
            <div class="comment">
                <div class="review">
                    <div class="text">Rating</div>
                    <span class="rating__star fa fa-star" >
                    </span>
                    <span class="rating__star fa fa-star">
                    </span>
                    <span class="rating__star fa fa-star">
                    </span>
                    <span class="rating__star fa fa-star">
                    </span>
                    <span class="rating__star fa fa-star">
                    </span>
                </div>
                <input type="hidden" class="count-start" name="rate" id="rate" value="5">
                <input type="hidden" name="book_id" id="book_id" value="<?= $book['book_id'] ?>">
                <input type="hidden" name="slug" id="slug" value="<?= $book['slug'] ?>">
                <textarea id="comment" name="comment" placeholder="insert your comment here"></textarea>
                <button class="button-submit">Submit</button>
            </div>
        </form>
    <?php endif ?>
    <?php if (count($review) > 0): ?>
        <?php for ($i = 0; $i < count($review); $i++) { ?>
            <div class="bubble-comment row">
                <div class="image col-lg-2 col-12">
                    <img src="<?= base_url() ?> /assets/userplaceholder.png">
                </div>
                <div class="comment-description col-lg-10 col-12">
                    <div class="name"><?= $review[$i]->user_fullname ?></div>
                    <div class="review">
                        <?php for ($j = 0; $j < $review[$i]->rating; $j++) { ?>
                            <span class="fa fa-star">
                            </span>
                        <?php } ?>
                        
                    </div>
                    <div class="description">
                        <p> <?= $review[$i]->review ?></p> 
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php else: ?>
        <div class="bubble-comment justify-content-md-center"> There's No Comment Here </div>
    <?php endif ?>
</div>
        
<script>
    document.getElementById("defaultOpen").click();
    const ratingStars = [...document.getElementsByClassName("rating__star")];
    const ratingResult = document.querySelector("#rate");
    function executeRating(stars, result) {
        const starClassActive = "rating__star fas fa-star";
        const starClassInactive = "rating__star far fa-star";
        const starsLength = stars.length;
        let i;
        stars.map((star) => {
            star.onclick = () => {
            i = stars.indexOf(star);
            j = 0

            if (star.className===starClassInactive) {
                printRatingResult(result, i + 1);
                for (i; i >= 0; --i) stars[i].className = starClassActive;
            } else {
                printRatingResult(result, i);
                for (i; i < starsLength; ++i) stars[i].className = starClassInactive;
            }
            };
        });
    }
    function printRatingResult(result, num = 0) {
        result.value = num;
    }
    executeRating(ratingStars, ratingResult)
</script>
<style>
body {
    display: flex;
    flex-direction: column;
}
/*  WRAPPER CONTENT*/
.wrapper-content {
    max-width: 1080px;
    margin: 0 auto 100px;
    padding: 0 16px;
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */
}
.wrapper-content::-webkit-scrollbar {
    display: none;
}
.wrapper-content .content {
   margin-top: 100px;
   padding-top: 64px;
}
.wrapper-content .content .wrapper-detail-book {
    display: flex;
    margin: 0;
}
.wrapper-content .content .wrapper-detail-book .image-book {
    display: flex;
    justify-content: center;
    margin-bottom: 16px;
    padding: 0;
    height: 390px;
}
.wrapper-content .content .wrapper-detail-book .image-book img {
    height: inherit;
    width: inherit;
    object-fit: cover;
}
.wrapper-content .content .wrapper-detail-book .detail-book {
    display: flex;
    flex-direction: column;
    padding: 0;
}
.wrapper-content .content .wrapper-detail-book .detail-book .title-book {
    font-size: 32px;
}
.wrapper-content .content .wrapper-detail-book .detail-book .author-book {
    font-size: 16px;
    margin-bottom: 8px;
}
.wrapper-content .content .wrapper-detail-book .detail-book .author-book .author-name{
    color: #d99879;
    cursor: pointer;
    font-weight: 600;
}
.wrapper-content .content .wrapper-detail-book .detail-book .author-book .author-name:hover{
    text-decoration: underline;
}
.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info{
    display: flex;
    margin-bottom: 32px;
}
.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info .rating{
    display: flex;
    justify-content: space-between;
}
.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info .rating .fa-star{
    font-size: 18px;
    color: #f9b208;
}

.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info .rating .fa-star-half{
    font-size: 18px;
    color: #f9b208;
}

.wrapper-content .content .wrapper-detail-book .detail-book .wrapper-detail-book-info .info-book {
    font-weight: 600;
}
.wrapper-content .content .wrapper-detail-book .detail-book .description-content {
    margin-bottom: 32px;
}



.wrapper-content .comment{
    margin-bottom: 16px;
    border: 1px solid #d99879;
    padding: 8px;
    border-radius: 5px;
}
.wrapper-content .comment .review{
    color: #f9b208;
    height: inherit;
    cursor: pointer;
    margin-bottom: 8px;
    display: flex;
    align-items: center;
}
.wrapper-content .comment .review .text{
    color: #000;
    margin-right: 16px;
}
.wrapper-content .comment .review .count-start{
    background-color: transparent;
    border: none;
    width: fit-content;
    /* display: none; */
}
.wrapper-content .comment .review .count-start:focus-visible{
    background-color: transparent;
    border: none;
}
.wrapper-content .comment .review .count-start::-webkit-outer-spin-button,
.wrapper-content .comment .review .count-start::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    background-color: transparent !important;
    border: none !important;
}
.wrapper-content .comment .button-submit{
    border: 1px solid #d99879;
    background-color: transparent;
    height: 32px;
    width: 100px;
    border-radius: 5px;
    font-weight: 600;
    color: #d99879;
}
.wrapper-content .comment .button-submit:active {
    background-color: #d99879;
    color: #fff;
}
.wrapper-content .comment .button-submit:focus{
    outline: none;
}
.wrapper-content .comment textarea{
    border: 1px solid #d99879;
    min-height: 50px;
    width: 100%;
    padding: 8px;
    border-radius: 5px;
}
.wrapper-content .comment textarea:focus{
    outline: none;
}

.wrapper-content .bubble-comment{
    border: 1px solid #d99879;
    border-radius: 5px;
    width: 100%;
    padding: 16px;
    display: flex;
    margin: 0;
    margin-bottom: 16px;
}
.wrapper-content .bubble-comment .image{
    border-radius: 5px;
    padding: 16px;
    display: flex;
    width: 132px;
    display: flex;
    justify-content: center;
}
.wrapper-content .bubble-comment .image img{
    height: inherit;
    width: inherit;
}
.wrapper-content .bubble-comment .comment-description .name{
    font-weight: 600;
    cursor: pointer;
}
.wrapper-content .bubble-comment .comment-description .review{
    color: #f9b208;
    height: inherit;
}
.wrapper-content .bubble-comment .comment-description .description{
    height: inherit;
}
/* TABS */
.tab {
    overflow: hidden;
    padding: 0 8px;
    margin-bottom: -2px;
    z-index: 2;
}
.tab button {
    color: #495057;
    border: none;
    background-color: transparent;
    padding: 0 8px;
    border-radius: 3px 3px 0 0;
    height: 32px;
}
.tab button.active{
    border-bottom: 3px solid #4a1c40;
    background-color: transparent;
    color: #495057;
    font-weight: 600;
    border: none;
}
.tab button:active{
    color: #495057;
}
.tab button:focus{
    outline: none;
}
.wrapper-tabs {
    width: 100%;
}
.tabcontent {
  display: none;
  padding: 8px 16px;
  border: 1px solid #d99879;
  border-radius: 5px;
}
@media (min-width: 768px) {
    .wrapper-content .content {
        margin-bottom: 64px;
    }
    .wrapper-content .content .wrapper-detail-book .detail-book {
        padding-left: 32px;
    }
}
@media (max-width: 768px) {
    .tabcontent {
        max-height: 400px;
        overflow-y: scroll;
    }
    .wrapper-content .bubble-comment .image{
        margin-bottom: 16px;
    }
}
</style>

<?= $this->endSection() ?>

<?= $this->section('script-custom') ?>

<script>
    function openTabs(evt, selectedTabs) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(selectedTabs).style.display = "block";
        evt.currentTarget.className += " active";
    }
    document.getElementById("defaultOpen").click();
</script>

<?= $this->endSection() ?>