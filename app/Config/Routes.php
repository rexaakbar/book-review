<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'HomeController::index', ["as" => "home"]);

$routes->get('login', 'AuthController::login', ["as" => "login"]);
$routes->post('login_store', 'AuthController::login_store', ["as" => "login_store"]);
$routes->get('register', 'AuthController::register', ["as" => "register"]);
$routes->post('register_store', 'AuthController::register_store', ["as" => "register_store"]);
$routes->get('logout_store', 'AuthController::logout_store', ["as" => "logout_store"]);

$routes->group('book', function ($routes) {
    $routes->get('detail/(:any)', 'BookController::detail/$1');
    $routes->get('list', 'BookController::list');
    $routes->get('list_filter', 'BookController::list_filter');
    $routes->post('submit_rate', 'BookController::submit_rate');
});

$routes->group('author', function ($routes) {
    $routes->get('detail/(:any)', 'AuthorController::detail/$1');
});


$routes->group('admin', function ($routes) {
    $routes->get('login', 'Admin\AuthController::login', ["as" => "admin.login"]);
    $routes->post('login_store', 'Admin\AuthController::login_store', ["as" => "admin.login_store"]);
    $routes->get('', 'Admin\DashboardController::index', ["as" => "admin.dashboard"]);
    $routes->get('logout_store', 'Admin\AuthController::logout_store', ["as" => "admin.logout_store"]);

    $routes->group('master', function ($routes) {
        $routes->get('category', 'Admin\Master\CategoryController::index', ["as" => "admin.master.category"]);
    });
});





/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
